theory evaluation

n OUT = 1/0/C: 6/4/0 
  Total      : 10 

From C1P1, C1P2: 

Number of multiple-covered cases: 0 

M1:    ~AUTONOMY + ~ASSET*~LEADERSHIP => ~AGENCY 

                       inclS  PRI    covS   covU   cases 
----------------------------------------------------------------- 
1  ~AUTONOMY           0.746  0.498  0.643  0.288  6; 1; 10; 8; 9 
2  ~ASSET*~LEADERSHIP  1.000  1.000  0.499  0.144  4 
----------------------------------------------------------------- 
   M1                  0.782  0.570  0.787 
